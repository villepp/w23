// src/App.js
import React from 'react';
import './App.css';
import Reporter from './Reporter';

function App() {
  return (
    <div className="App">
      <Reporter name="Antero Mertaranta">Löikö mörkö sisään</Reporter>
      <Reporter name="Kevin McGran">I know it's a rough time now, but did you at least enjoy playing in the tournament</Reporter>
    </div>
  );
}

export default App;
