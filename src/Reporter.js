import React from 'react';

const Reporter = ({ name, children }) => {
  const imageSize = {
    width: '300px',
    height: 'auto'
  };

  return (
    <div>
      {children === 'Löikö mörkö sisään' && (
        <img src="morko.jpeg" alt="Mörkö" style={imageSize} />
      )}
      <p>{name}: {children}</p>
    </div>
  );
};

export default Reporter;
